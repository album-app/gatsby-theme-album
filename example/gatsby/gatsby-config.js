module.exports = {
  siteMetadata: {
    title: 'My solutions',
    subtitle: 'Exemplary album catalog.',
    catalog_url: 'https://gitlab.com/MY_NAME/example',
    catalog_meta: `${__dirname}/../album_catalog_index.json`,
    menuLinks:[
      {
         name:'home',
         link:'/'
      },
      {
         name:'Catalog',
         link:'/catalog'
      },
      {
         name:'About',
         link:'/about'
      },
      {
         name:'Docs',
         link:'/docs'
      }
    ]
  },
  plugins: [{ resolve: `gatsby-theme-album`, options: {} },],
}
