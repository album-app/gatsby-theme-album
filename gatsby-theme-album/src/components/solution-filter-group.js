import React from "react"
const SolutionFilterGroup = ({ name, active, children }) => {
  return (
  <div><div>filter by {name}</div>
    <div>{children}</div>
  </div>
)}
export default SolutionFilterGroup