import React from "react"
import { Link } from "gatsby"
const LinkList = ({ items, prepath }) => {
    return (
  <>
    <ul>
      {items.map(item => (
        <li key={item}>
            <Link to={prepath + item}>{item}</Link>
        </li>
      ))}
    </ul>
  </>
)}
export default LinkList