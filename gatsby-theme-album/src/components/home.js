import React from "react"
import {Link} from "gatsby"

const Home = () => (
  <div>Welcome! Please visit the <Link to="/catalog">catalog</Link> to get an overview of our solutions deployed via album.</div>
)
export default Home