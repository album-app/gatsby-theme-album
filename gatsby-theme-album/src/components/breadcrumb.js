import React from "react"
import { globalHistory } from "@reach/router"
import { Link } from "gatsby"

const Breadcrumb = ({ location }) => {
    var path = globalHistory.location.pathname.trim().split("/")
    path.shift()
    if(path[0] === "") path = []
    return (
    <>
      <div>
        <Link key="home" to="/"><strong>Home</strong></Link>
      {path.map((crumb, index) => {
          const crumbPath = "/" + path.slice(0, index+1).join("/")
          return (
            <> : <Link key={"b_"+index} to={crumbPath}><strong>{crumb}</strong> </Link></>
          )}
      )}
      </div>
    </>
    )
}

export default Breadcrumb