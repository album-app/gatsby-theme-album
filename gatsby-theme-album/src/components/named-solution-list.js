import React from "react"
import SolutionList from "./solution-list"

const NamedSolutionList = ({ name, solutions }) => {
    return(
    <>
      <h2>{name}</h2>
      <SolutionList solutions={solutions}/>
    </>
    )
}
export default NamedSolutionList