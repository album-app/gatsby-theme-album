import React from "react"
import { Link } from "gatsby"
const PageList = ({ pages }) => (
  <>
    <h2>Pages</h2>
    <ul>
      {pages.map(page => (
        <li key={page.id}>
            <Link to={page.frontmatter.permalink}>{page.frontmatter.title}</Link>
        </li>
      ))}
    </ul>
  </>
)
export default PageList