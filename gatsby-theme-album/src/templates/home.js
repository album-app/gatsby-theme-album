import React from "react"
import Layout from "../components/layout"
import Home from "../components/home"

const HomeTemplate = ({pageContext}) => {
  return (
    <Layout site={pageContext.site}>
      <Home/>
    </Layout>
  )
}

export default HomeTemplate