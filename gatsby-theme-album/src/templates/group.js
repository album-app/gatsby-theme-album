import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import LinkList from "../components/link-list"

export const query = graphql`
  query($name: String!) {
     nameFields: allSqliteSolution(filter:{group:{eq:$name}}) {
        group(field:name) {
            fieldValue
        }
    }
}
`
const GroupTemplate = ({ data: { nameFields }, pageContext }) => {
    const items = []
    nameFields.group.forEach(name => {
        items.push(name.fieldValue)
    })
    return (
      <Layout site={pageContext.site}>
            <h1>Solutions by {pageContext.name}</h1>
            <LinkList prepath={"/"+pageContext.name+"/"} items={items}/>
      </Layout>
    )
}

export default GroupTemplate