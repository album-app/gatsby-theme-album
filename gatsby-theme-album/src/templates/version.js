import React from "react"
import Layout from "../components/layout"
import Solution from "../components/solution"

const VersionTemplate = ({ pageContext }) => {
    return (
      <Layout site={pageContext.site} path={pageContext.path}>
        <Solution {...pageContext} />
      </Layout>
    )
}

export default VersionTemplate