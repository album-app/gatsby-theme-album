import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Page from "../components/page"

export const query = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { permalink: { eq: $path } }) {
      html
      frontmatter {
        permalink
        title
      }
    }
  }
`
const PageTemplate = ({ data: { markdownRemark }, pageContext }) => (
  <Layout site={pageContext.site}>
    <Page {...markdownRemark} />
  </Layout>
)

export default PageTemplate