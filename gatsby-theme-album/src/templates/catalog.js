import React from "react"
import Layout from "../components/layout"
import SolutionList from "../components/solution-list"

const CatalogTemplate = ({ pageContext }) => {
    return (
      <Layout site={pageContext.site}>
        <SolutionList solutions={pageContext.solutions} tags={pageContext.tags} />
      </Layout>
    )
}

export default CatalogTemplate